
  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
  import { getDatabase, onValue ,ref, set, child, get, update, remove } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
  import { getAuth, signInWithEmailAndPassword, signOut, onAuthStateChanged } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";

  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyCH3Bv6Ztdo2VWHoV5gQFsyTUlUgCefdig",
    authDomain: "carlos-omar.firebaseapp.com",
    databaseURL: "https://carlos-omar-default-rtdb.firebaseio.com",
    projectId: "carlos-omar",
    storageBucket: "carlos-omar.appspot.com",
    messagingSenderId: "554817962829",
    appId: "1:554817962829:web:09937b6c20f6c8485bad04"
  };



  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db = getDatabase();

  //----------------------------------------------------------
  var btnLoguear = document.getElementById('btnLoguear');
  var btnDisconnect = document.getElementById('btnDisconnect');
  var llenado = document.getElementById('llenar');
  var comprobar = document.getElementById('comprobarCuenta');
  var paginaFULL = document.getElementById('paginaADMIN')

  var email = "";
  var password = "";



  const auth = getAuth();

  function leer(){
  email = document.getElementById('correo').value;
  password = document.getElementById('pass').value;
  }




  // Permite ver si el usuario esta conectado o no
  function comprobarAUTH(){
    onAuthStateChanged(auth, (user) => {
      if (user) {
        alert("Usuario detectado");
        document.getElementById('todo').style.display="block";
        // ...
      } else {
        // User is signed out
        // ...
        alert("No se ha detectado un usuario");
        window.location.href="https://carlos2020030397.w3spaces.com/html/index-p002.html";
      }
    });
  }


  // Si existen = entonces hacer acción

  if(window.location.href == "https://carlos2020030397.w3spaces.com/html/admin.html"){
    window.onload = comprobarAUTH();
  }



  if(btnLoguear){
    btnLoguear.addEventListener('click', (e)=>{
      leer();
      signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
          // Signed in 
          const user = userCredential.user;
          alert("¡INICIO DE SESIÓN EXITOSO!")
          window.location.href="admin.html";
          // ...
        })
        .catch((error) => {
          alert("DATOS INCORRECTOS")
          const errorCode = error.code;
          const errorMessage = error.message;
        });
    });

  }


  if(btnDisconnect){
    btnDisconnect.addEventListener('click',  (e)=>{
      signOut(auth).then(() => {
        alert("¡CERRAR SESIÓN EXITOSO!")
        window.location.href="https://omarosuna.000webhostapp.com/html/index-p002.html";
        // Sign-out successful.
      }).catch((error) => {
        // An error happened.
      });
    });
  }


  // Para crear contenido en la pag de admin

  function Rellamar(){
    if(btnDisconnect){
      btnDisconnect.addEventListener('click',  (e)=>{
        signOut(auth).then(() => {
          alert("¡CERRAR SESIÓN EXITOSO!")
          window.location.href="https://omarosuna.000webhostapp.com/html/index-p002.html";
          // Sign-out successful.
        }).catch((error) => {
          // An error happened.
        });
      });
    }

    if(comprobar){
      comprobar.addEventListener('click', (e)=>{
      const user = auth.currentUser;
      if (user !== null) {
        const email = user.email;
        alert(email);
    }
    });
  }

  }
